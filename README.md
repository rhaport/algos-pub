### GENERAL INFORMATION ###
Every team member have to implement two algorithms per week! 

Challenge start: 06-11-2017
Challenge end: 10-12-2017

The algorithms should be implemented in C++ along with unit-tests verifying the correctness of the implemented algorithms. 

The week goal is reached if the algorithms passes all unit-tests at 23:59:59 of Sunday. 

Other team members are allowed to implement their own unit-tests trying to let the implementation fail.

Deadline for pushing unit-tests is 21:00 of Sunday.

### CODE STRUCTURE ###
the code tree looks like this

```html
<pre>
algos
  |
  `--algorithms                        # all algorithms should be implemented there
  |   |
  |   `--<algortihm name>              # each algorithm should be impleemnted in a separated directory
  |   |
  |   |	  `--src                       # source files (including headers) should be located in the src directory
  |   |	  |   |
  |   |	  |   `--somefile.hpp
  |   |   |   `--somefile.cpp
  |   |   |   
  |   |   `--test                      # unit-tests should be placed here
  |   |   |   |
  |   |   |   `--some_unittest.cpp
  |   |   |
  |   |   `--Makefile                  # each algorithm have own Makefile. However it is implemented quite generic 
  |   |                                # and could be copy-pasted to other algorithm's directories
  |   |
  |   `--<algorithm name>
  |       |
  |       `--...
  |
  `--common
  |   |
  |   `-- # some common stuff for all algorithms 
  |
  `--Makefile  # make files used by pipeplines. Normally should not be updated. It just calls make commandos for
               # for each subdirectory in algorithms directory 
  
</pre>	   
```

### CODE VERIFICATION ###
Pipeline compiles all sources and unit-tests at each commit

### AND NOW...###
– Поехали!
