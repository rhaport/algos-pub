/*
 * test_quicksort.cpp, week 03
 *
 *  Created on: Nov 20, 2017
 *      Author: rha
 */

#include "quicksort.hpp"

#include "algos_common.hpp"
#include "test_sort_common.hpp"

#include <vector>

/****************************************************************************
 * NAMESPACE
 ****************************************************************************/
using namespace algos;

/****************************************************************************
 * EXPORTED FUNCTIONS
 ****************************************************************************/
int main(int argc, char **argv)
{
    {
        std::vector<int> v =  {3, 4};
        test_sort(v, quicksort);
    }

    {
        std::vector<int> v =  {4, 3};
        test_sort(v, quicksort);
    }

    {
        std::vector<int> v1 =  {2, 5, 1, 6, 9, 8, 0, 3, 7, 4};
        test_sort(v1, quicksort);
    }

    {
        std::vector<int> v2 =  {2, 5, 1, 6, 9, 8, 0, 3, 7, 4, 15, 10, 12, 14, 11};
        test_sort(v2, quicksort);
    }

    {
        std::vector<int> v3 = {1};
        test_sort(v3, quicksort);
    }

    {
        std::vector<int> v4 = {4, 1};
        test_sort(v4, quicksort);
    }

    {
        std::vector<int> v5 = {4, 1, 7, 0, 9, 2, 0};
        test_sort(v5, quicksort);
    }

    {
        std::vector<int> v6 = {};
        test_sort(v6, quicksort);
    }

    {
        std::vector<int> v7 = {1, 1, 1, 1};
        test_sort(v7, quicksort);
    }
}



