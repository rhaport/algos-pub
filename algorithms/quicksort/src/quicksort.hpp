/*
 * quicksort.hpp, week 03
 *
 *  Created on: Nov 20, 2017
 *      Author: rha
 */

#if !defined(ALGORITHMS_QUICKSORT_SRC_QUICKSORT_HPP_)
#define ALGORITHMS_QUICKSORT_SRC_QUICKSORT_HPP_

/******************************************************************************
 * INCLUDES
 ******************************************************************************/
#include "algos_common.hpp"
#include <iterator>
#include <stdint.h>
#include <stack>

/******************************************************************************
 * NAMESPACE
 ******************************************************************************/

/******************************************************************************
 * EXPORTED FUNCTIONS
 ******************************************************************************/
namespace algos
{
/**
 * @brief Hoare partition scheme
 */
template<class BiDirIterator>
BiDirIterator partition(
        BiDirIterator left,
        BiDirIterator right,
        typename std::iterator_traits<BiDirIterator>::value_type pivot)
{
    BiDirIterator li = left, ri = std::prev(right);

    while (true)
    {
        while (*li < pivot)
        {
            ++li;
        }

        while (*ri > pivot)
        {
            --ri;
        }

        if (li < ri)
        {
            std::swap(*li, *ri);
            ++li;
            --ri;
        }
        else
        {
            if (ri == std::prev(right))
            {
                --ri;
            }
            break;
        }
    }

    return ri;
}
/**
 * @brief quicksort algorithm implementation
 */
template<class BiDirIterator>
void quicksort_recursive(BiDirIterator first, BiDirIterator last)
{
    if (first == last) return;

    if (first < std::prev(last))
    {
        typename std::iterator_traits<BiDirIterator>::value_type pivot;
        pivot = (*first + *(last - 1)) / 2;
        BiDirIterator middle = algos::partition(first, last, pivot);
        quicksort(first, middle + 1);
        quicksort(middle + 1, last);
    }
}

/**
 * @brief quicksort algorithm implementation
 */
template<class BiDirIterator>
void quicksort(BiDirIterator first, BiDirIterator last)
{
    if (first == last) return;

    std::stack<BiDirIterator> stack;
    BiDirIterator middle;

    while (true)
    {
        while (first < std::prev(last))
        {
            typename std::iterator_traits<BiDirIterator>::value_type pivot;
            pivot = (*first + *(last - 1)) / 2;
            middle = algos::partition(first, last, pivot);
            stack.push(last);
            last = middle + 1;
        }

        if (stack.empty())
        {
            break;
        }
        else
        {
            first = middle + 1;
            last = stack.top();
            stack.pop();
        }
    }
}

} // END: algos

#endif /* ALGORITHMS_QUICKSORT_SRC_QUICKSORT_HPP_ */
