/*
 * matrix.cpp
 *
 *  Created on: Nov 25, 2017
 *      Author: rha
 */

#include "matrix.hpp"

/*****************************************************************************
 * NAMESPACES
 *****************************************************************************/
using namespace algos;

/******************************************************************************
 * @brief comparison operator for double
 ******************************************************************************/
bool algos::operator==(const Matrix<double> &lhs, const Matrix<double> &rhs)
{
    bool res;
    size_t const cols = lhs.cols();
    size_t const rows = lhs.rows();
    double const EPSILON = 1e-20;

    if (rhs.rows() != rows || rhs.cols() != cols)
    {
        res = false;
    }
    else
    {
        res = true;

        for (size_t i = 0; (i < rows) && res; ++i)
        {
            for (size_t j = 0; (j < cols) && res; ++j)
            {
                res = std::fabs(lhs(i, j) - rhs(i, j)) <= EPSILON;
            }
        }
    }

    return res;
}
