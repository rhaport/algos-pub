/*
 * square_matrix.hpp, week 02
 *
 *  Created on: Nov 18, 2017
 *      Author: rha
 */

#if !defined(ALGORITHMS_ROTATE_SRC_SQUARE_MATRIX_HPP_)
#define ALGORITHMS_ROTATE_SRC_SQUARE_MATRIX_HPP_

/******************************************************************************
 * INCLUDES
 ******************************************************************************/
#include "matrix.hpp"

/******************************************************************************
 * FORWARD CLASS DECLARATION
 ******************************************************************************/

/******************************************************************************
 * CLASS DEFINITION
 ******************************************************************************/
namespace algos
{
template<class Type>
class SquareMatrix : public Matrix<Type>
{
public:
    explicit SquareMatrix(size_t n) : Matrix<Type>(n, n) {}
    explicit SquareMatrix(size_t n, Type **raw_data);
    SquareMatrix(const SquareMatrix<Type> &other);
    SquareMatrix(Matrix<Type> &&other);

    virtual ~SquareMatrix() {}

    /**
     * @brief rotate matrix in 90 degree steps
     *
     * @details this method rotate the content of the matrix in 90 degree
     *          steps clock-wise or anti-clock-wise
     *
     * @param quadrants - number of 90 degree rotations. might also be negative
     */
    void rotate(int quadrants);

    /**
     * @brief transpose matrix
     */
    void transpose();

private:
    /**
     * @brief get number of minimum rotations needed to rotate matrix by
     *        requested number of quadrants
     */
    unsigned get_minimum_rotations(int quadrants)
    {
        unsigned min_rotations;

        // since rotations have period 4, it is enough to consider quadrants
        // modulo 4
        if (quadrants < 0)
        {
            min_rotations = 4 - ((-quadrants) & 3);
        }
        else
        {
            min_rotations = quadrants & 3;
        }

        return min_rotations;
    }
};

/******************************************************************************
 * @brief constructor which initializes matrix from the square array
 ******************************************************************************/
template<class Type>
SquareMatrix<Type>::SquareMatrix(size_t n, Type **raw_data)
        : Matrix<Type>(n, n, raw_data)
{
}

/******************************************************************************
 * @brief copy constructor
 ******************************************************************************/
template<class Type>
SquareMatrix<Type>::SquareMatrix(const SquareMatrix<Type> &other)
        : Matrix<Type>(other)
{
}

/******************************************************************************
 * @brief move constructor
 ******************************************************************************/
template<class Type>
SquareMatrix<Type>::SquareMatrix(Matrix<Type> &&other)
        : Matrix<Type>(other)
{
}

/******************************************************************************
 * @brief rotate matrix in 90 degree steps
 ******************************************************************************/
template<class Type>
void SquareMatrix<Type>::rotate(int quadrants)
{
    unsigned const rotations = get_minimum_rotations(quadrants);
    size_t const n = this->rows();
    size_t const half_n = n >> 1;
    SquareMatrix<Type> &matrix = *this;

    // clock-wise rotation by 90 degree
    if (1 == rotations)
    {
        // always start with an element on the main diagonal
        // and slice along one side of the middle square and swap elements
        // on its 4 sides
        for (size_t i = 0; i < half_n; ++i)
        {
            for (size_t dx = 0; dx < (n - 2*i - 1); ++dx)
            {
                size_t j = i + dx;
                Type tmp = matrix(i, j);
                matrix(i, j) = matrix(n - j - 1, i);
                matrix(n - j - 1, i) = matrix(n - i - 1, n - j - 1);
                matrix(n - i - 1, n - j - 1) = matrix(j, n - i - 1);
                matrix(j, n - i - 1) = tmp;
            }
        }
    }
    // anti-clock-wise rotation by 90 degree
    else if (3 == rotations)
    {
        // like by rotations == 1, but swap elements on the square sides
        // in the opposite direction
        for (size_t i = 0; i < half_n; ++i)
        {
            for (size_t dx = 0; dx < (n - 2*i - 1); ++dx)
            {
                size_t j = i + dx;
                Type tmp = matrix(i, j);
                matrix(i, j) = matrix(j, n - i - 1);
                matrix(j, n - i - 1) = matrix(n - i - 1, n - j - 1);
                matrix(n - i - 1, n - j - 1) = matrix(n - j - 1, i);
                matrix(n - j - 1, i) = tmp;
            }
        }

    }
    // rotation by 180 degree in either direction
    else if (2 == rotations)
    {
        // this rotation turns to swapping the top and bottom lines
        // with changing the direction
        // like this:
        // a_11 a_12 a_13 ... a_1n       a_nn ... a_n3 a_n2 a_n1
        // .......................  -->  .......................
        // a_n1 a_n2 a_n3 ... a_nn       a_1n ... a_13 a_12 a_11
        for (size_t i = 0; i < half_n; ++i)
        {
            for (size_t j = 0; j < n; ++j)
            {
                Type tmp = matrix(i, j);
                matrix(i, j) = matrix(n - i - 1, n - j - 1);
                matrix(n - i - 1, n - j - 1) = tmp;
            }
        }

        // in case of odd number of rows, the middle row should be
        // treated separately, because only one row should rotated
        if (1 == (n & 1))
        {
            for (size_t j = 0; j < half_n; ++j)
            {
                Type tmp = matrix(half_n, j);
                matrix(half_n, j) = matrix(half_n, n - j - 1);
                matrix(half_n, n - j - 1) = tmp;
            }
        }
    }
}

/******************************************************************************
 * @brief transpose matrix
 ******************************************************************************/
template<class Type>
void SquareMatrix<Type>::transpose()
{
    size_t const n = this->rows();

    // nothing to do for 1x1 or zero matrices
    if (n < 2) return;

    SquareMatrix<Type> &matrix = *this;

    for (size_t i = 0; i < n - 1; ++i)
    {
        for (size_t j = 1; j < n; ++j)
        {
            Type tmp = matrix(i, j);
            matrix(i, j) = matrix(j, i);
            matrix(j, i) = tmp;
        }
    }
}

} // END: algos

#endif /* ALGORITHMS_ROTATE_SRC_SQUARE_MATRIX_HPP_ */
