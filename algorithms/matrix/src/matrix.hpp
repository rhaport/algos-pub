#if !defined(ALGOS_MATRIX_HPP_)
#define ALGOS_MATRIX_HPP_

/*****************************************************************************
 * INCLUDES
 *****************************************************************************/
#include <assert.h>
#include <cmath>
#include <cstddef>
#include <cstring>
#include <ostream>
#include <vector>

/*****************************************************************************
 * FORWARD CLASS DECLARATION
 *****************************************************************************/
namespace algos
{
template<class Type>
class Matrix;

// spezialization for double
bool operator==(const Matrix<double> &lhs, const Matrix<double> &rhs);
}

/*****************************************************************************
 * CLASS DEFINITION
 *****************************************************************************/
namespace algos
{
/**
 * @brief class representing a generic matrix
 */
template<class Type>
class Matrix
{
public:
    explicit Matrix(size_t rows, size_t cols);
    explicit Matrix(size_t rows, size_t cols, Type **raw_data);
    Matrix(const Matrix<Type> &m);
    Matrix(Matrix<Type> &&other);
    virtual ~Matrix() { delete[] m_matrix; }

    size_t rows() const { return m_num_rows; }
    size_t cols() const { return m_num_cols; }

    Type& operator()(size_t i, size_t j);
    const Type& operator()(size_t i, size_t j) const;
    Matrix<Type>& operator=(Matrix<Type> &&other);

    template <class T>
    friend Matrix<T> operator*(const Matrix<T> &lhs, const Matrix<T> &rhs);

    friend bool operator==(const Matrix<double> &lhs, const Matrix<double> &rhs);

    template <class T>
    friend bool operator==(const Matrix<T> &lhs, const Matrix<T> &rhs);

    template <class T>
    friend std::ostream& operator<<(std::ostream &os, const Matrix<T> &m);

protected:
    Type *data() { return m_matrix; }

private:
    Type *m_matrix;
    size_t m_num_rows;
    size_t m_num_cols;
};

/******************************************************************************
 * @brief matrix constructor
 ******************************************************************************/
template<class Type>
Matrix<Type>::Matrix(size_t rows, size_t cols)
        : m_matrix(nullptr), m_num_rows(rows), m_num_cols(cols)
{
    size_t total_size = rows * cols;

    m_matrix = new Type[total_size];
    memset(m_matrix, 0, sizeof(Type) * total_size);
}

/******************************************************************************
 * @brief copy constructor
 ******************************************************************************/
template<class Type>
Matrix<Type>::Matrix(const Matrix<Type> &m)
        : m_num_rows(m.m_num_rows), m_num_cols(m.m_num_cols)
{
    size_t total_size = m_num_rows * m_num_cols;
    m_matrix = new Type[total_size];

    memcpy(m_matrix, m.m_matrix, total_size * sizeof(Type));
}

/******************************************************************************
 * @brief move constructor
 ******************************************************************************/
template<class Type>
Matrix<Type>::Matrix(Matrix<Type> &&other)
        : m_num_rows(other.m_num_rows), m_num_cols(other.m_num_cols)
{
    m_matrix = other.m_matrix;
    other.m_matrix = nullptr;
}


/******************************************************************************
 * @brief init from array constructor
 ******************************************************************************/
template<class Type>
Matrix<Type>::Matrix(size_t rows, size_t cols, Type **raw_array)
        : m_num_rows(rows), m_num_cols(cols)
{
    size_t total_size = m_num_rows * m_num_cols;
    m_matrix = new Type[total_size];

    memcpy(m_matrix, raw_array, total_size * sizeof(Type));
}

/******************************************************************************
 * @brief parenthesis operator
 ******************************************************************************/
template<class Type>
Type& Matrix<Type>::operator()(size_t i, size_t j)
{
    assert(i < m_num_rows);
    assert(j < m_num_cols);
    size_t const plain_idx = i*m_num_cols + j;

    return m_matrix[plain_idx];
}

/******************************************************************************
 * @brief parenthesis operator
 ******************************************************************************/
template<class Type>
const Type& Matrix<Type>::operator()(size_t i, size_t j) const
{
    assert(i < m_num_rows);
    assert(j < m_num_cols);
    size_t const plain_idx = i*m_num_cols + j;

    return m_matrix[plain_idx];
}

/******************************************************************************
 * @brief move assignment operator
 ******************************************************************************/
template<class Type>
Matrix<Type>& Matrix<Type>::operator=(Matrix<Type> &&other)
{
    if (this != &other)
    {
        this->m_matrix = other.m_matrix;
        this->m_num_cols = other.m_num_cols;
        this->m_num_rows = other.m_num_rows;
        other.m_matrix = nullptr;
    }

    return *this;
}

/******************************************************************************
 * @brief multiplication operator
 ******************************************************************************/
template<class T>
Matrix<T> operator*(const Matrix<T> &lhs, const Matrix<T> &rhs)
{
    size_t const res_row_size = lhs.cols();
    assert(res_row_size == rhs.rows());

    size_t const res_rows = lhs.rows();
    size_t const res_cols = rhs.cols();
    Matrix<T> res(res_rows, res_cols);

    for (size_t i = 0; i < res_rows; ++i)
    {
        for (size_t j = 0; j < res_cols; ++j)
        {
            for (size_t k = 0; k < res_row_size; ++k)
            {
                res(i, j) += lhs(i, k) * rhs(k, j);
            }
        }
    }

    return res;
}


/******************************************************************************
 * @brief comparison operator
 ******************************************************************************/
template<class T>
bool operator==(const Matrix<T> &lhs, const Matrix<T> &rhs)
{
    bool res;
    size_t const cols = lhs.cols();
    size_t const rows = lhs.rows();

    if (rhs.rows() != rows || rhs.cols() != cols)
    {
        res = false;
    }
    else
    {
        res = true;

        for (size_t i = 0; (i < rows) && res; ++i)
        {
            for (size_t j = 0; (j < cols) && res; ++j)
            {
                res = lhs(i, j) == rhs(i, j);
            }
        }
    }

    return res;
}


/******************************************************************************
 * @brief output operator
 ******************************************************************************/
template<class T>
std::ostream& operator<<(std::ostream &os, const Matrix<T> &m)
{
    for (size_t i = 0; i < m.m_num_rows; ++i)
    {
        for (size_t j = 0; j < m.m_num_cols; ++j)
        {
            os << m(i, j) << " ";
        }
        os << std::endl;
    }

    return os;
}
} // END: algos

#endif
