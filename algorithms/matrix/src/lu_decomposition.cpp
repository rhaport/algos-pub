/*
 * lu_decomposition.cpp, week04
 *
 *  Created on: Nov 27, 2017
 *      Author: rha
 */
/******************************************************************************
 * INCLUDES
 ******************************************************************************/
#include "lu_decomposition.hpp"

#include <cmath>
#include <stdexcept>

/*****************************************************************************
 * NAMESPACES
 *****************************************************************************/
using namespace algos;

/*****************************************************************************
 * CLASS METHODS
 *****************************************************************************/
/*****************************************************************************
 * @brief constructor. initially copy matrix as-is
 *****************************************************************************/
LuDecomposition::LuDecomposition(const RealMatrix &m) : m_lu(m)
{
    size_t n = m.rows();
    m_permutation.resize(n);

    for (size_t i = 0; i < n; ++i)
    {
        m_permutation[i] = i;
    }

    do_decomposition();
}

/*****************************************************************************
 * @brief do actual decomposition
 *****************************************************************************/
void LuDecomposition::do_decomposition()
{
    size_t const n = m_lu.rows();

    for (size_t k = 0; k < n; ++k)
    {
        size_t pivot_row = find_pivot_row(k);
        if (k != pivot_row)
        {
            swap_rows(k, pivot_row);
        }

        // if the pivot element is zero, then the matrix is singular!
        if (std::fabs(m_lu(pivot_row, k)) <= EPSILON)
        {
            throw std::runtime_error("singular matrix");
        }

        // perform Gaussian elimination
        for (size_t i = k + 1; i < n; ++i)
        {
            m_lu(i, k) /= m_lu(k, k);
            for (size_t j = k + 1; j < n; ++j)
            {
                m_lu(i, j) -= m_lu(i, k)*m_lu(k, j);
            }
        }

    }
}

/*****************************************************************************
 * @brief find row with maximum (absolute value) element on the diagonal
 *****************************************************************************/
size_t LuDecomposition::find_pivot_row(size_t col)
{
    size_t const n = m_lu.rows();
    size_t pivot = col;
    double abs_max = std::fabs(m_lu(pivot, col));

    for (size_t i = col + 1; i < n; ++i)
    {
        double val = std::fabs(m_lu(i, col));
        if (val > abs_max)
        {
            abs_max = val;
            pivot = i;
        }
    }

    return pivot;
}

/*****************************************************************************
 * @brief swap rows in the decomposition matrix
 *****************************************************************************/
void LuDecomposition::swap_rows(size_t row1, size_t row2)
{
    if (row1 == row2) return;

    // swap rows
    for (size_t i = 0; i < m_lu.cols(); ++i)
    {
        std::swap(m_lu(row1, i), m_lu(row2, i));
    }

    // do also swap in the permutation vector
    std::swap(m_permutation[row1], m_permutation[row2]);
}




