/*
 * lu_decomposition.hpp
 *
 *  Created on: Nov 27, 2017
 *      Author: rha
 */

#if !defined(ALGORITHMS_MATRIX_SRC_LU_DECOMPOSITION_HPP_)
#define ALGORITHMS_MATRIX_SRC_LU_DECOMPOSITION_HPP_

/******************************************************************************
 * INCLUDES
 ******************************************************************************/
#include "square_matrix.hpp"

#include <vector>

/******************************************************************************
 * NAMESPACE
 ******************************************************************************/

/******************************************************************************
 * EXPORTED FUNCTIONS
 ******************************************************************************/
namespace algos
{
/**
 * @brief LU decomposition of square matrix. The representation A = LU gives
 *        significant speed up is solving of linear equations when the linear
 *        equations of form Ax = b with many different b's but the same A
 *        need to be solved
 *
 *        The decomposition happens in-place in order to save the memory
 *        As result we get a matrix in form L + U - E where L is lower
 *        triangle matrix with ones on diagonal, U is upper triangle matrix
 *        and E is identity matrix
 *
 *        General idea is that the matrix A (say 4x4) can be multiplied by
 *        special lower triangular matrix L1 in the form
 *
 *             ( 1    0  0  0)
 *             ( c_10 1  0  0)
 *        L1 = ( c_20 0  1  0) where c_i0 = -a_i0/a_00
 *             ( c_30 0  0  1)
 *
 *        then
 *               ( a'_00 a'_01 a'_02 a'_03)
 *               ( 0     a'_11 a'_12 a'_13)
 *        L1*A = ( 0     a'_21 a'_22 a'_23)
 *               ( 0     a'_31 a'_32 a'_33)
 *
 *        this process can be repeated by multiplying with L2, L3, etc.
 *        At the end we get
 *
 *        U = L3*L2*L1*A  =>    L1^-1 * L2^-1 * L3^-1 * A
 *
 *        note that the Li^-1 can be easily obtained from Li by changing the
 *        sign of the elements in the non-zero column. And the result of
 *        multiplication of lower triangle matrix will be again lower triangle
 *        matrix
 *
 */
class LuDecomposition
{
public:
    typedef SquareMatrix<double> RealMatrix;

    LuDecomposition(const RealMatrix &m);
    ~LuDecomposition() {}

    const RealMatrix& get_raw_matrix() const { return m_lu; }

private:
    /**
     * @brief do actual decomposition
     *
     * @details this method should be called from the constructor to perform
     *          the decomposition. as result the m_decomposition matrix should
     *          be represented as L + U - E where L is lower triangle matrix
     *          with ones on diagonal, U is upper triangle matrix and E
     *          is identity matrix.
     */
    void do_decomposition();

    /**
     * @brief find row with maximum (absolute value) element on the given column
     *
     * @details this method is called during decomposition to find the maximum
     *          element on the given column starting from the diagonal in order
     *          to improve the stability of the algorithm
     */
    size_t find_pivot_row(size_t col);

    /**
     * @brief swap rows in the decomposition matrix
     */
    void swap_rows(size_t row1, size_t row2);


    RealMatrix m_lu;
    std::vector<size_t> m_permutation;
    double const EPSILON = 1e-20;
};
}

#endif /* ALGORITHMS_MATRIX_SRC_LU_DECOMPOSITION_HPP_ */
