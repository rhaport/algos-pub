/*
 * test_mul.cpp, week03
 *
 *  Created on: Nov 25, 2017
 *      Author: rha
 */
/*****************************************************************************
 * INCLUDES
 *****************************************************************************/
#include "matrix.hpp"
#include "square_matrix.hpp"

#include <assert.h>
#include <iostream>

/*****************************************************************************
 * NAMESPACES
 *****************************************************************************/
using namespace algos;

/*****************************************************************************
 * @brief main routine
 *****************************************************************************/
int main(int argc, char **argv)
{
    {
        int m1_array[2][3] = {
                {1, 2, 3},
                {4, 5, 6}};

        int m2_array[3][2] = {
                {1, 0},
                {1, 1},
                {0, 1}};

        int ref_array[2][2] = {
                {3, 5},
                {9, 11}};

        Matrix<int> m1(2, 3, reinterpret_cast<int**>(m1_array));
        Matrix<int> m2(3, 2, reinterpret_cast<int**>(m2_array));
        SquareMatrix<int> m_ref(2, reinterpret_cast<int**>(ref_array));

        // function under test
        SquareMatrix<int> test = m1 * m2;
        assert(test == m_ref);
    }

    return 0;
}




