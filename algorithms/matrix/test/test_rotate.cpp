/*
 * test_rotate.cpp, week02
 *
 *  Created on: Nov 18, 2017
 *      Author: rha
 */
/*****************************************************************************
 * INCLUDES
 *****************************************************************************/
#include "square_matrix.hpp"

#include <assert.h>
#include <iostream>

/*****************************************************************************
 * NAMESPACES
 *****************************************************************************/
using namespace algos;

/*****************************************************************************
 * @brief main routine
 *****************************************************************************/
int main(int argc, char **argv)
{
    {
        int raw_array[3][3] = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}};

        int ref_array[3][3] = {
                {7, 4, 1},
                {8, 5, 2},
                {9, 6, 3}};

        SquareMatrix<int> m_test(3, reinterpret_cast<int**>(raw_array));
        SquareMatrix<int> m_ref(3, reinterpret_cast<int**>(ref_array));

        // function under test
        m_test.rotate(1);
        assert(m_test == m_ref);
    }

    {
        int raw_array[3][3] = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}};

        int ref_array[3][3] = {
                {7, 4, 1},
                {8, 5, 2},
                {9, 6, 3}};

        SquareMatrix<int> m_test(3, reinterpret_cast<int**>(raw_array));
        SquareMatrix<int> m_ref(3, reinterpret_cast<int**>(ref_array));

        // function under test
        m_test.rotate(-3);
        assert(m_test == m_ref);
    }

    {
        int raw_array[3][3] = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}};

        int ref_array[3][3] = {
                {3, 6, 9},
                {2, 5, 8},
                {1, 4, 7}};

        SquareMatrix<int> m_test(3, reinterpret_cast<int**>(raw_array));
        SquareMatrix<int> m_ref(3, reinterpret_cast<int**>(ref_array));

        // function under test
        m_test.rotate(-1);
        assert(m_test == m_ref);
    }

    {
        int raw_array[3][3] = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}};

        int ref_array[3][3] = {
                {3, 6, 9},
                {2, 5, 8},
                {1, 4, 7}};

        SquareMatrix<int> m_test(3, reinterpret_cast<int**>(raw_array));
        SquareMatrix<int> m_ref(3, reinterpret_cast<int**>(ref_array));

        // function under test
        m_test.rotate(3);
        assert(m_test == m_ref);
    }

    {
        int raw_array[3][3] = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}};

        int ref_array[3][3] = {
                {9, 8, 7},
                {6, 5, 4},
                {3, 2, 1}};

        SquareMatrix<int> m_test(3, reinterpret_cast<int**>(raw_array));
        SquareMatrix<int> m_ref(3, reinterpret_cast<int**>(ref_array));

        // function under test
        m_test.rotate(2);

        assert(m_test == m_ref);
    }

    return 0;
}

