/*
 * test_lu_decomposition.cpp, week04
 *
 *  Created on: Nov 28, 2017
 *      Author: rha
 */
/*****************************************************************************
 * INCLUDES
 *****************************************************************************/
#include "lu_decomposition.hpp"

#include <assert.h>
#include <iostream>

/*****************************************************************************
 * NAMESPACES
 *****************************************************************************/
using namespace algos;

/*****************************************************************************
 * @brief main routine
 *****************************************************************************/
int main(int argc, char **argv)
{
    {
        double m_array[2][2] = {
                {2, 5},
                {1, 3}};

        double lu_array[2][2] = {
                {2,     5},
                {0.5,   0.5}};


        SquareMatrix<double> m(2, reinterpret_cast<double**>(m_array));
        SquareMatrix<double> ref(2, reinterpret_cast<double**>(lu_array));

        LuDecomposition lu(m);

        assert(lu.get_raw_matrix() == ref);
    }
}




