/*
 * created by rha, Dec 7, 2017
 * algo challenge, week04
 */
/*****************************************************************************
 * INCLUDES
 *****************************************************************************/
#include "square_matrix.hpp"

/*****************************************************************************
 * NAMESPACES
 *****************************************************************************/
using namespace algos;

/*****************************************************************************
 * main function
 *****************************************************************************/
int main(int argc, char **argv)
{
    {
        int raw_array[3][3] = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}};

        int ref_array[3][3] = {
                {1, 4, 7},
                {2, 5, 8},
                {3, 6, 9}};

        SquareMatrix<int> m_test(3, reinterpret_cast<int**>(raw_array));
        SquareMatrix<int> m_ref(3, reinterpret_cast<int**>(ref_array));

        // function under test
        m_test.transpose();
        assert(m_test == m_ref);
    }

    {
        int raw_array[2][2] = {
                {1, 2},
                {4, 5}};

        int ref_array[2][2] = {
                {1, 4},
                {2, 5}};

        SquareMatrix<int> m_test(2, reinterpret_cast<int**>(raw_array));
        SquareMatrix<int> m_ref(2, reinterpret_cast<int**>(ref_array));

        // function under test
        m_test.transpose();
        assert(m_test == m_ref);
    }

}
