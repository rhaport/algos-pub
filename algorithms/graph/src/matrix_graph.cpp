/*
 * created by rha, Dec 7, 2017
 * algo challenge, week04
 */
/*****************************************************************************
 * INCLUDES
 *****************************************************************************/
#include "matrix_graph.hpp"

/*****************************************************************************
 * NAMESPACES
 *****************************************************************************/
using namespace algos;

/*****************************************************************************
 * CLASS METHODS
 *****************************************************************************/
/*****************************************************************************
 * @brief constructor
 *****************************************************************************/
MatrixGraph::MatrixGraph(const std::vector<t_rawAdjacentList> &source)
        : GraphIf(), m_graph(source.size())
{
    size_t const n = source.size();

    for (size_t i = 0; i < n; ++i)
    {
        for (size_t j = 0; j < source[i].size(); ++j)
        {
            m_graph(i, source[i][j]) = 1;
        }
    }
}

/*****************************************************************************
 * @brief get in degree
 *****************************************************************************/
int MatrixGraph::get_in_degree(const Vertex &v) const
{
    size_t const n = m_graph.rows();
    size_t const col = v.get_value();
    size_t degree = 0;

    for (size_t i = 0; i < n; ++i)
    {
        degree += m_graph(i, col);
    }

    return degree;
}

/*****************************************************************************
 * @brief get out degree
 *****************************************************************************/
int MatrixGraph::get_out_degree(const Vertex &v) const
{
    size_t const n = m_graph.rows();
    size_t const row = v.get_value();
    size_t degree = 0;

    for (size_t i = 0; i < n; ++i)
    {
        degree += m_graph(row, i);
    }

    return degree;
}

/*****************************************************************************
 * @brief reverse
 *****************************************************************************/
void MatrixGraph::reverse()
{
    m_graph.transpose();
}

/******************************************************************************
 * @brief output operator
 *****************************************************************************/
std::ostream &algos::operator<<(std::ostream &os, const MatrixGraph &g)
{
    os << g.m_graph;
    return os;
}
