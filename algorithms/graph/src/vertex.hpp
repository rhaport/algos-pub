/*
 * vertex.hpp
 *
 *  Created on: Dec 6, 2017
 *      Author: rha
 */

#if !defined(ALGORITHMS_GRAPH_SRC_VERTEX_HPP_)
#define ALGORITHMS_GRAPH_SRC_VERTEX_HPP_

/******************************************************************************
 * INCLUDES
 ******************************************************************************/

/******************************************************************************
 * FORWARD CLASS DECLARATION
 ******************************************************************************/

/******************************************************************************
 * CLASS DEFINITION
 ******************************************************************************/
namespace algos
{
class Vertex
{
public:
    Vertex() : value(), visited() {}
    explicit Vertex(int value) : value(value), visited(false) {}
    virtual ~Vertex() {}

    int get_value() const { return value; }
    void refresh() { visited = false; }

    Vertex& operator=(int value) { this->value = value; this->visited = false; return *this; }
    bool operator==(const Vertex &rhs) const { return this->value == rhs.value; }

private:
    int value;
    bool visited;
};
}

#endif /* ALGORITHMS_GRAPH_SRC_VERTEX_HPP_ */
