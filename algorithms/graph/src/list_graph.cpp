/*
 * created by rha, Dec 3, 2017
 * algo challenge, week04
 */
/*****************************************************************************
 * INCLUDES
 *****************************************************************************/
#include "list_graph.hpp"

/*****************************************************************************
 * NAMESPACES
 *****************************************************************************/
using namespace algos;

/*****************************************************************************
 * CLASS METHODS
 *****************************************************************************/
/*****************************************************************************
 * @brief constructor
 *****************************************************************************/
ListGraph::ListGraph(const std::vector<t_rawAdjacentList> &source)
        : m_graph()
{
    m_graph.resize(source.size());

    for (size_t i = 0; i < source.size(); ++i)
    {
        m_graph[i].vertex = i;
        for (size_t j = 0; j < source[i].size(); ++j)
        {
            m_graph[i].list.push_back(Vertex(source[i][j]));
        }
    }
}

/*****************************************************************************
 * @brief get in degree
 *****************************************************************************/
int ListGraph::get_in_degree(const Vertex &v) const
{
    int degree = 0;

    for (size_t i = 0; i < m_graph.size(); ++i)
    {
        for (size_t j = 0; j < m_graph[i].list.size(); ++j)
        {
            if (m_graph[i].list[j] == v)
            {
                ++degree;
            }
        }
    }

    return degree;
}

/*****************************************************************************
 * @brief get out degree
 *****************************************************************************/
int ListGraph::get_out_degree(const Vertex &v) const
{
    return m_graph[v.get_value()].list.size();
}

/*****************************************************************************
 * @brief reverse directed graph
 *****************************************************************************/
void ListGraph::reverse()
{
    std::vector<AdjacentList> new_graph(m_graph.size());

    for (auto it = m_graph.begin(); it != m_graph.end(); ++it)
    {
        AdjacentList &adj_list = *it;

        for (auto adj_it = adj_list.list.begin();
                adj_it != adj_list.list.end();
                ++adj_it)
        {
            new_graph[adj_it->get_value()].list.push_back(it->vertex);
            new_graph[adj_it->get_value()].vertex = *adj_it;
        }
    }

    m_graph = new_graph;
}

/******************************************************************************
 * @brief output operator
 *****************************************************************************/
std::ostream &algos::operator<<(std::ostream &os, const ListGraph &g)
{
    for (auto v = g.m_graph.begin(); v != g.m_graph.end(); ++v)
    {
        os << v->vertex.get_value() << " | ";
        for (auto l = v->list.begin(); l != v->list.end(); ++l)
        {
            os << l->get_value() << " ";
        }

        os << std::endl;
    }

    return os;
}
