/*
 * list_graph.hpp
 * algos challenge, week05
 *
 *  Created on: Dec 3, 2017
 *      Author: rha
 */

#if !defined(ALGORITHMS_GRAPH_SRC_LIST_GRAPH_HPP_)
#define ALGORITHMS_GRAPH_SRC_LIST_GRAPH_HPP_

/******************************************************************************
 * INCLUDES
 ******************************************************************************/
#include "graph_if.hpp"
#include "vertex.hpp"

#include <list>
#include <ostream>
#include <vector>

/******************************************************************************
 * FORWARD CLASS DECLARATION
 ******************************************************************************/
namespace algos
{
class Vertex;
class ListGraph;
std::ostream &operator<<(std::ostream &os, const ListGraph &g);
}
/******************************************************************************
 * CLASS DEFINITION
 ******************************************************************************/
namespace algos
{
class ListGraph : public GraphIf
{
public:
    struct AdjacentList
    {
        Vertex vertex;
        std::vector<Vertex> list;
    };

    ListGraph(const std::vector<t_rawAdjacentList> &source);
    virtual ~ListGraph() {}

    virtual int get_out_degree(const Vertex &v) const;
    virtual int get_in_degree(const Vertex &v) const;
    virtual void reverse();

    friend std::ostream &operator<<(std::ostream &os, const ListGraph &g);

private:
    std::vector<AdjacentList> m_graph;
};

} // END: algos

#endif /* ALGORITHMS_GRAPH_SRC_LIST_GRAPH_HPP_ */
