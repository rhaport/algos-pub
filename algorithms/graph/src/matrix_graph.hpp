/*
 * matrix_graph.hpp_
 *
 *  Created on: Dec 7, 2017
 *      Author: rha
 */

#if !defined(ALGORITHMS_GRAPH_SRC_MATRIX_GRAPH_HPP_)
#define ALGORITHMS_GRAPH_SRC_MATRIX_GRAPH_HPP_

/******************************************************************************
 * INCLUDES
 ******************************************************************************/
#include "graph_if.hpp"
#include "square_matrix.hpp"
#include "vertex.hpp"

/******************************************************************************
 * FORWARD CLASS DECLARATION
 ******************************************************************************/
namespace algos
{
class MatrixGraph;
std::ostream &operator<<(std::ostream &os, const MatrixGraph &g);
}
/******************************************************************************
 * CLASS DEFINITION
 ******************************************************************************/
namespace algos
{
class MatrixGraph : public GraphIf
{
public:
    MatrixGraph(const std::vector<t_rawAdjacentList> &source);
    ~MatrixGraph() {}

    virtual int get_out_degree(const Vertex &v) const;
    virtual int get_in_degree(const Vertex &v) const;
    virtual void reverse();

    friend std::ostream &operator<<(std::ostream &os, const MatrixGraph &g);

private:
    SquareMatrix<int> m_graph;
};
} // END: algos

#endif /* ALGORITHMS_GRAPH_SRC_MATRIX_GRAPH_HPP_ */
