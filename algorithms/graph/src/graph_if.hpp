/*
 * graph_if.hpp
 * algos challenge, week05
 *
 *  Created on: Dec 3, 2017
 *      Author: rha
 */

#if !defined(ALGORITHMS_GRAPH_SRC_GRAPH_IF_HPP_)
#define ALGORITHMS_GRAPH_SRC_GRAPH_IF_HPP_

/******************************************************************************
 * INCLUDES
 ******************************************************************************/
#include <vector>

/******************************************************************************
 * FORWARD CLASS DECLARATION
 ******************************************************************************/
namespace algos
{
class Vertex;
}
/******************************************************************************
 * CLASS DEFINITION
 ******************************************************************************/
namespace algos
{
class GraphIf
{
public:
    typedef std::vector<int> t_rawAdjacentList;

    virtual ~GraphIf() {}

    virtual int get_out_degree(const Vertex &v) const = 0;
    virtual int get_in_degree(const Vertex &v) const = 0;
    virtual void reverse() = 0;
};
} // END: algos
#endif /* ALGORITHMS_GRAPH_SRC_GRAPH_IF_HPP_ */
