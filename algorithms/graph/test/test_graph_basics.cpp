/*
 * created by rha, Dec 3, 2017
 * algo challenge, week05
 */
/*****************************************************************************
 * INCLUDES
 *****************************************************************************/
#include "list_graph.hpp"
#include "matrix_graph.hpp"
#include <assert.h>
#include <iostream>

/*****************************************************************************
 * NAMESPACES
 *****************************************************************************/
using namespace algos;

void test_in_out_degree(GraphIf &graph, const Vertex &v, int out_degree,
        int in_degree);

/*****************************************************************************
 * @brief main function
 *****************************************************************************/
int main(int argc, char **argv)
{
    {
        std::vector<ListGraph::t_rawAdjacentList> src =
            {
                { 1, 2 },
                { 0, 2 },
                { 1 }
            };

        ListGraph list_graph(src);
        test_in_out_degree(list_graph, Vertex(2), 2, 1);
        std::cout << list_graph << std::endl;
    }

    {
        std::vector<ListGraph::t_rawAdjacentList> src =
            {
                { 1, 2, 3 },
                { 0, 2 },
                { 1, 0, 3 },
                { 3 }
            };

        MatrixGraph matrix_graph(src);

        test_in_out_degree(matrix_graph, Vertex(2), 2, 3);
        std::cout << matrix_graph << std::endl;
    }
}

void test_in_out_degree(GraphIf &graph, const Vertex &v, int in_degree,
        int out_degree)
{
    static int ntest = 0;
    std::cout << "Graph test #" << ++ntest << std::endl;

    assert(graph.get_in_degree(v) == in_degree);
    assert(graph.get_out_degree(v) == out_degree);

    graph.reverse();

    assert(graph.get_in_degree(v) == out_degree);
    assert(graph.get_out_degree(v) == in_degree);
}
