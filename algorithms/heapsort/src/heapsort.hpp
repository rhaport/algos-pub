/*
 * algo challenge. week02
 *
 * heapsort implementation
 *
 *  Created on: Nov 15, 2017
 *      Author: rha
 */

#if !defined(ALGOS_HEAPSORT_HPP_)
#define ALGOS_HEAPSORT_HPP_

/****************************************************************************
 * INCLUDE
 ****************************************************************************/
#include "algos_common.hpp"
#include <bits/stl_iterator_base_types.h>

/****************************************************************************
 * CLASS FORWARD DECLARATIONS
 ****************************************************************************/

/****************************************************************************
 * LOCAL FUNCTIONS
 ****************************************************************************/

/****************************************************************************
 * EXPORTED FUNCTIONS
 ****************************************************************************/
namespace algos
{
/**
 * @brief this function maintains the max-heap property
 *
 * @details it is assumed that left and right subtrees of the current
 *          element are max-heaps. The current element "floats down"
 *          the heap so that the subtree rooted at current item obeys
 *          the max-heap property. The running time of the algorithm is
 *          O(log(h)) where h is the height of the current node
 *
 * @param current - to be heapified element of the container
 * @param last element of the container
 */
template<class BiDirIterator>
void heapify(BiDirIterator first, BiDirIterator item, BiDirIterator last)
{
    typedef typename
            std::iterator_traits<BiDirIterator>::difference_type t_distance;

    BiDirIterator largest = item, current{};
    t_distance order = std::distance(first, item) + 1;

    do
    {
        current = largest;

        // determine whether left child is bigger than parent
        if (std::distance(current, last) > order)
        {
            BiDirIterator left = current + order;
            if (*left > *largest)
            {
                largest = left;
            }
        }

        // determine whether right child is bigger than parent
        if (std::distance(current, last) > order + 1)
        {
            BiDirIterator right = current + order + 1;
            if (*right > *largest)
            {
                largest = right;
            }
        }

        // is parent was not the largest repeat the procedure with the largest
        // child
        if (largest != current)
        {
            order += std::distance(current, largest);
            std::swap(*largest, *current);
        }
    } while (largest != current);
}

/**
 * @brief build max heap
 *
 * @details
 */
template<class BiDirIterator>
void build_heap(BiDirIterator first, BiDirIterator last)
{
    if (first == last) return;

    typedef typename
            std::iterator_traits<BiDirIterator>::difference_type t_distance;

    t_distance length = std::distance(first, last);
    BiDirIterator current = first + length/2 - (length > 1);

    while (current != first)
    {
        heapify(first, current, last);
        --current;
    }

    heapify(first, first, last);
}


/**
 * @brief heapsort for std containers
 *
 * @details
 */
template<class BiDirIterator>
void heapsort(BiDirIterator first, BiDirIterator last)
{
    if (first == last) return;

    // build max heap
    build_heap(first, last);

    BiDirIterator item = std::prev(last);

    while (item != first)
    {
        std::swap(*item, *first);
        heapify(first, first, item);
        std::advance(item, -1);
    }
}


}

#endif /* ALGOS_HEAPSORT_HPP_ */
