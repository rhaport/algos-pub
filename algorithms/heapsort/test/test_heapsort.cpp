/****************************************************************************
 * INCLUDE
 ****************************************************************************/
#include "heapsort.hpp"

#include "algos_common.hpp"
#include "test_sort_common.hpp"

#include <vector>

/****************************************************************************
 * NAMESPACE
 ****************************************************************************/
using namespace algos;

/****************************************************************************
 * EXPORTED FUNCTIONS
 ****************************************************************************/
int main(int argc, char **argv)
{
    {
        std::vector<int> v =  {2, 5, 1, 6, 9, 8, 0, 3, 7, 4};
        test_sort(v, heapsort);
    }

    {
        std::vector<int> v =
                {2, 5, 1, 6, 9, 8, 0, 3, 7, 4, 15, 10, 12, 14, 11};
        test_sort(v, heapsort);
    }

    {
        std::vector<int> v = {1};
        test_sort(v, heapsort);
    }

    {
        std::vector<int> v4 = {4, 1};
        test_sort(v4, heapsort);
    }

    {
        std::vector<int> v5 = {4, 1, 7, 0, 9, 2, 0};
        test_sort(v5, heapsort);
    }

    {
        std::vector<int> v6 = {};
        test_sort(v6, heapsort);
    }
}
