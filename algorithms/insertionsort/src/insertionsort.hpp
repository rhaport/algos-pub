/*
 * algo challenge, week 01
 *
 * insertionsort.hpp
 *
 *  Created on: Nov 11, 2017
 *      Author: rha
 */

#if !defined(ALGORITHMS_INSERTIONSORT_SRC_INSERTIONSORT_HPP_)
#define ALGORITHMS_INSERTIONSORT_SRC_INSERTIONSORT_HPP_

/******************************************************************************
 * INCLUDES
 ******************************************************************************/
#include "algos_common.hpp"
#include <bits/stl_iterator_base_types.h>

/******************************************************************************
 * FORWARD CLASS DECLARATION
 ******************************************************************************/

/******************************************************************************
 * EXPORTED FUNCTIONS
 ******************************************************************************/
namespace algos
{
template<class BiDirIterator>
void insertionsort(BiDirIterator first, BiDirIterator last)
{
    typedef typename
            std::iterator_traits<BiDirIterator>::value_type
            t_valueType;

    if (first == last) return;

    for (BiDirIterator it = first + 1; it != last; ++it)
    {
        t_valueType key = *it;

        BiDirIterator sorted_it = it - 1;
        // insert key into the sorted sequence preceding the key
        while (sorted_it != first && *sorted_it > key)
        {
            *(sorted_it + 1) = *sorted_it;
            --sorted_it;
        }

        if (sorted_it == first && *first > key)
        {
            // we reached first element and it is still larger than key
            *(first + 1) = *first;
            *first = key;
        }
        else
        {
            *(sorted_it + 1) = key;
        }
    }
}
}

#endif /* ALGORITHMS_INSERTIONSORT_SRC_INSERTIONSORT_HPP_ */
