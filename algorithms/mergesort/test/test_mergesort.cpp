/****************************************************************************
 * INCLUDE
 ****************************************************************************/
#include "mergesort.hpp"

#include "algos_common.hpp"
#include "test_sort_common.hpp"

#include <vector>

/****************************************************************************
 * NAMESPACE
 ****************************************************************************/
using namespace algos;

/****************************************************************************
 * EXPORTED FUNCTIONS
 ****************************************************************************/
int main(int argc, char **argv)
{
    std::vector<int> v1 =  {2, 5, 1, 6, 9, 8, 0, 3, 7, 4};
    test_sort(v1, mergesort);

    std::vector<int> v2 =  {2, 5, 1, 6, 9, 8, 0, 3, 7, 4, 15, 10, 12, 14, 11};
    test_sort(v2, mergesort);

    std::vector<int> v3 = {1};
    test_sort(v3, mergesort);

    std::vector<int> v4 = {4, 1};
    test_sort(v4, mergesort);

    std::vector<int> v5 = {4, 1, 7, 0, 9, 2, 0};
    test_sort(v5, mergesort);

    std::vector<int> v6 = {};
    test_sort(v6, mergesort);

    {
        std::vector<int> v7 = { 0, 1, 2, 3, 4, 5, 6 };
        decltype(v7) v7_ref = v7;

        mergesort(std::begin(v7) + 3,
                  std::begin(v7) + 1);

        (v7 == v7_ref)
            ? [](){}()
            : [](){
                std::exit(EXIT_FAILURE);
            }();
    }
}
