/*
 * algo challenge. week01
 *
 * mergesort implementation
 *
 *  Created on: Nov 10, 2017
 *      Author: rha
 */

#ifndef ALGOS_MERGESORT_HPP_
#define ALGOS_MERGESORT_HPP_

/****************************************************************************
 * INCLUDE
 ****************************************************************************/
#include "algos_common.hpp"
#include <bits/stl_iterator_base_types.h>
#include <bits/stl_algo.h>
#include <vector>

/****************************************************************************
 * CLASS FORWARD DECLARATIONS
 ****************************************************************************/

/****************************************************************************
 * LOCAL FUNCTIONS
 ****************************************************************************/

/****************************************************************************
 * EXPORTED FUNCTIONS
 ****************************************************************************/
namespace algos
{
/**
 * @brief in-place merge of two parts
 *
 * @details this algorithm uses O(n) additional space and time
 *          complexity is O(n). It appeared that the merge algorithm with
 *          O(1) additional space is very complicated. Even std doesn't
 *          implement it.
 */
template<class BiDirIterator>
void merge(BiDirIterator first, BiDirIterator mid, BiDirIterator last)
{
    typedef typename
            std::iterator_traits<BiDirIterator>::value_type
            t_valueType;

    BiDirIterator result = first;
    std::vector<t_valueType> left(first, mid);
    std::vector<t_valueType> right(mid, last);

    typename std::vector<t_valueType>::iterator left_iter = left.begin();
    typename std::vector<t_valueType>::iterator right_iter = right.begin();

    while (left_iter != left.end() && right_iter != right.end())
    {
        t_valueType right_value, left_value;

        right_value = *right_iter;
        left_value = *left_iter;

        if (left_value <= right_value)
        {
            *result = left_value;
            ++left_iter;
        }
        else
        {
            *result = right_value;
            ++right_iter;
        }
        ++result;
    }

    while (left_iter != left.end())
    {
        *result = *left_iter;
        ++left_iter;
        ++result;
    }

    while (right_iter != right.end())
    {
        *result = *right_iter;
        ++right_iter;
        ++result;
    }
}

/**
 * @brief mergesort for std containers
 *
 * @details
 */
template<class BiDirIterator>
void mergesort(BiDirIterator first, BiDirIterator last)
{
    if (first >= last) return;

    size_t size = last - first;

    // choose step for merge left and right parts of sub-arrays of size @step
    for (size_t step = 2; step <= size; step <<= 1)
    {
        size_t i;

        // index of the first element of each sub-array
        for (i = 0; i <= (size - step); i += step)
        {
            BiDirIterator left = first + i;
            BiDirIterator mid = left + (step / 2);
            BiDirIterator right = left + step;

            //std::inplace_merge(left, mid, right);
            merge(left, mid, right);
        }

        dump_container(first, last);

        // last merge if the size of the array was not divisible by step
        if (i < size)
        {
            //std::inplace_merge(first + i - step, first + i, last);
            merge(first + i - step, first + i, last);
        }
    }
}
}



#endif /* ALGOS_MERGESORT_HPP_ */
