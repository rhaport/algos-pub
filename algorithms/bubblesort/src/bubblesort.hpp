/*
 * algo challenge. week01
 *
 * bubblesort implementation
 *
 *  Created on: Nov 7, 2017
 *      Author: rha
 */

#if !defined(ALGOS_BUBBLESORT_HPP_)
#define ALGOS_BUBBLESORT_HPP_

/****************************************************************************
 * INCLUDE
 ****************************************************************************/
#include "algos_common.hpp"
#include <bits/stl_iterator_base_types.h>

/****************************************************************************
 * CLASS FORWARD DECLARATIONS
 ****************************************************************************/

/****************************************************************************
 * EXPORTED FUNCTIONS
 ****************************************************************************/
namespace algos
{
template<class Iterator>
void bubblesort(Iterator first, Iterator last)
{
    bool hasSwapped;

    if (first == last) return;

    do
    {
        hasSwapped = false;

        for (Iterator it = first; it != last - 1; ++it)
        {
            typename 
            std::iterator_traits<Iterator>::value_type val, val_next;
            
            val = *it;
            val_next = *(it + 1);

            if (val_next < val)
            {
                *it = val_next;
                *(it + 1) = val;
                hasSwapped = true;
            }
        }
    } while (hasSwapped);
}
}

#endif /* ALGOS_BUBBLESORT_HPP_ */ 
