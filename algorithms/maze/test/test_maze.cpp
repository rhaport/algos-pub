/*
 * test_maze.cpp, week04
 *
 *  Created on: Nov 30, 2017
 *      Author: rha
 */
/*****************************************************************************
 * INCLUDES
 *****************************************************************************/
#include "maze.hpp"

#include <assert.h>
#include <iostream>
#include <string>

/*****************************************************************************
 * NAMESPACES
 *****************************************************************************/
using namespace algos;

/*****************************************************************************
 * STATIC FUNCTIONS
 *****************************************************************************/
static void test(const std::vector<std::string> &input_maze,
        const Maze::Point &from,
        const Maze::Point &to,
        uint32_t expected_length);

static void test_not_found(const std::vector<std::string> &input_maze,
        const Maze::Point &from,
        const Maze::Point &to);

/*****************************************************************************
 * STATIC DATA
 *****************************************************************************/
static std::vector<std::string> const INPUT_MAZE_0 =
{   //y: 01234567     x
        "XXX.....", //0
        "..X.....", //1
        "..XXXX.X", //2
        "........", //3
        "....X...", //4
        "....X...", //5
        "....X...", //6
};

static std::vector<std::string> const INPUT_MAZE_1 =
{   //y: 012345     x
        "XXX...", //0
        "..X...", //1
        "..XXXX", //2
        "......", //3
        "..X...", //4
};

static size_t s_test_num = 0;

/*****************************************************************************
 * @brief main routine
 *****************************************************************************/
int main(int argc, char **argv)
{
    test(INPUT_MAZE_0, Maze::Point(1, 1), Maze::Point(1, 3), 12);
    test(INPUT_MAZE_0, Maze::Point(1, 1), Maze::Point(1, 0), 1);
    test_not_found(INPUT_MAZE_0, Maze::Point(1, 1), Maze::Point(1, 2));
    test(INPUT_MAZE_0, Maze::Point(2, 1), Maze::Point(6, 5), 8);
    test(INPUT_MAZE_0, Maze::Point(2, 1), Maze::Point(6, 7), 10);

    test(INPUT_MAZE_1, Maze::Point(4, 5), Maze::Point(1, 0), 8);
    test_not_found(INPUT_MAZE_1, Maze::Point(1, 1), Maze::Point(1, 3));
}

/*****************************************************************************
 * @brief test engine
 *****************************************************************************/
void test(const std::vector<std::string> &input_maze,
        const Maze::Point &from,
        const Maze::Point &to,
        uint32_t expected_length)
{
    ++s_test_num;

    std::cout << "Test No. " << s_test_num << std::endl;

    Maze maze(input_maze);

    Maze::Point const A = from;
    Maze::Point const B = to;
    Maze::Route route;

    bool const is_found = maze.find_route(A, B, route);

    assert(is_found);
    assert(route.size() == expected_length);

    if (is_found)
    {
        maze.put_on(A, 'A');
        maze.put_on(B, 'B');
        maze.put_on(A, route, 'o');
        maze.dump();
    }

    std::cout << "PASSED" << std::endl;
}

static void test_not_found(const std::vector<std::string> &input_maze,
        const Maze::Point &from,
        const Maze::Point &to)
{
    ++s_test_num;

    std::cout << "Test No. " << s_test_num << std::endl;

    Maze maze(input_maze);

    Maze::Point const A = from;
    Maze::Point const B = to;
    Maze::Route route;

    bool const is_found = maze.find_route(A, B, route);

    assert(false == is_found);

    std::cout << "PASSED" << std::endl;

}
