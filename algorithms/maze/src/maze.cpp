/*
 * created by rha, Nov 30, 2017
 * algo challenge, week04
 */
/*****************************************************************************
 * INCLUDES
 *****************************************************************************/
#include "maze.hpp"

#include <iterator>
#include <iostream>
#include <queue>

/*****************************************************************************
 * NAMESPACES
 *****************************************************************************/
using namespace algos;

/*****************************************************************************
 * CLASS METHODS
 *****************************************************************************/
/*****************************************************************************
 * @brief constructor
 *****************************************************************************/
Maze::Maze(const std::vector<std::string> &str_maze)
        : m_height(str_maze.size())
        , m_width(m_height ? str_maze[0].size() : 0)
{
   // NOTE: to simplify algorithm, ensure that the maze is rectangle
   for (auto it = str_maze.begin(); it != str_maze.end(); ++it)
   {
       assert(it->size() == m_width);
       m_grid.push_back(*it);
   }
}

/*****************************************************************************
 * @brief destructor
 *****************************************************************************/
Maze::~Maze()
{
}

/*****************************************************************************
 * @brief find route
 *****************************************************************************/
bool Maze::find_route(const Point &from, const Point &to, Route &route)
{
    bool is_found = false;
    bool is_possible = true;
    // note that array is always zeroed at initialization
    Matrix<int> search_grid(m_height, m_width);

    if (is_reachable(from) && is_reachable(to))
    {
        route.clear();
        // start counting from 1 since the matrix is filled with zeros
        search_grid(from.x, from.y) = 1;
    }
    else
    {
        is_possible = false;
    }

    std::queue<Point> q;
    Point point(from);

    while (is_possible && !is_found)
    {
        if (point == to)
        {
            is_found = true;
            break;
        }

        Point neighbours[4] = {
                up(point),
                down(point),
                left(point),
                right(point)};

        for (size_t i = 0; i < 4; ++i)
        {
            const Point &n = neighbours[i];
            if (is_reachable(n) && 0 == search_grid(n.x, n.y))
            {
                // each reachable and non-visited point is add to the queue
                // and marked as visited
                q.push(n);
                search_grid(n.x, n.y) = search_grid(point.x, point.y) + 1;
            }
        }

        if (q.empty())
        {
            is_possible = false;
        }
        else
        {
            point = q.front();
            q.pop();
        }
    }

    if (is_found)
    {
        // the path is found. build the route
        point = to;

        while (point != from)
        {
            Point tmp;
            int state = search_grid(point.x, point.y) - 1;
            Direction next_direction;

            if ((tmp = left(point), is_reachable(tmp))
                    && search_grid(tmp.x, tmp.y) == state)
            {
                next_direction = DIRECTION_RIGHT;
            }
            else if ((tmp = right(point), is_reachable(tmp))
                    && search_grid(tmp.x, tmp.y) == state)
            {
                next_direction = DIRECTION_LEFT;
            }
            else if ((tmp = up(point), is_reachable(tmp))
                    && search_grid(tmp.x, tmp.y) == state)
            {
                next_direction = DIRECTION_DOWN;
            }
            else if ((tmp = down(point), is_reachable(tmp))
                    && search_grid(tmp.x, tmp.y) == state)
            {
                next_direction = DIRECTION_UP;
            }
            else
            {
                // something wrong
                assert(0);
            }

            route.push_front(next_direction);
            point = tmp;
        }
    }

    return is_found;
}

/*****************************************************************************
 * @brief find route
 *****************************************************************************/
bool Maze::is_reachable(const Point &point)
{
    bool const res = ((0 <= point.x && point.x < static_cast<int>(m_height))
            && (0 <= point.y && point.y < static_cast<int>(m_width))
            && m_grid[point.x][point.y] != 'X');
    return res;
}

/*****************************************************************************
 * @brief mark point on a grid
 *****************************************************************************/
void Maze::put_on(const Point &point, char ch)
{
    if (is_reachable(point))
    {
        m_grid[point.x][point.y] = ch;
    }
}

void Maze::put_on(const Point &point, const Route &route, char ch)
{
    if (route.begin() == route.end() || route.begin() == std::prev(route.end()))
    {
        return;
    }

    Point p = point;
    auto it = route.begin();
    auto last = std::prev(route.end());

    while (is_reachable(p) && it != last)
    {
        switch (*it)
        {
        case DIRECTION_DOWN:
            p = down(p);
            break;

        case DIRECTION_UP:
            p = up(p);
            break;

        case DIRECTION_LEFT:
            p = left(p);
            break;

        case DIRECTION_RIGHT:
            p = right(p);
            break;
        }

        assert(is_reachable(p));
        m_grid[p.x][p.y] = ch;
        ++it;
    }
}

void Maze::dump()
{
    std::cout << "[Maze]" << std::endl;
    for (auto it = m_grid.begin(); it != m_grid.end(); ++it)
    {
        std::cout << *it << std::endl;
    }
}
