/*
 * maze.hpp
 *
 *  Created on: Nov 30, 2017
 *      Author: rha
 *
 *  algo challenge, week04
 */

#if !defined(ALGORITHMS_MAZE_SRC_MAZE_HPP_)
#define ALGORITHMS_MAZE_SRC_MAZE_HPP_

/******************************************************************************
 * INCLUDES
 ******************************************************************************/
#include "matrix.hpp"

#include <cstdint>
#include <list>
#include <string>
#include <vector>

/******************************************************************************
 * FORWARD CLASS DECLARATION
 ******************************************************************************/

/******************************************************************************
 * CLASS DEFINITION
 ******************************************************************************/
namespace algos
{
class Maze
{
public:
    enum Direction : uint8_t
    {
        DIRECTION_UP,
        DIRECTION_RIGHT,
        DIRECTION_DOWN,
        DIRECTION_LEFT
    };

    typedef std::list<Direction> Route;

    struct Point
    {
        int x;
        int y;

        Point() : x(), y() {}
        Point(int xx, int yy) : x(xx), y(yy) {}

        bool operator==(const Point &rhs)
        {
            return (x == rhs.x) && (y == rhs.y);
        }

        bool operator!=(const Point &rhs)
        {
            return (x != rhs.x) || (y != rhs.y);
        }
    };

    Maze(const std::vector<std::string> &str_maze);
    ~Maze();

    /**
     * @brief find shortest route from $from to $to
     *
     * @param from - start point in maze
     * @param to - finish point in maze
     * @param route - set of directions from start to finish. if the route
     *                was not found, the content is unspecified
     */
    bool find_route(const Point &from, const Point &to, Route &route);

    void put_on(const Point &point, char ch);
    void put_on(const Point &point, const Route &route, char ch);

    void dump();

private:
    bool is_reachable(const Point &point);
    Point up(const Point &p) { return Point(p.x - 1, p.y); }
    Point down(const Point &p) { return Point(p.x + 1, p.y); }
    Point right(const Point &p) { return Point(p.x, p.y + 1); }
    Point left(const Point &p) { return Point(p.x, p.y - 1); }

    std::vector<std::string> m_grid;
    size_t const m_height;
    size_t const m_width;
};
} // END: algos

#endif /* ALGORITHMS_MAZE_SRC_MAZE_HPP_ */
