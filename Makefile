CC=g++
MAKE=make

OUT_DIR=out
COMMON_DIR=common
ALGOS_DIRS=$(dir $(wildcard algorithms/*/.))
ALGOS_TEMP=$(foreach dir,$(ALGOS_DIRS),$(subst algos/,,${dir}))
ALGOS=$(foreach algo,$(ALGOS_TEMP),$(subst /,,${algo}))

$(info $(ALGOS_DIRS))

all:
	for target in $(ALGOS_DIRS); do\
		echo $$target; \
		make -C $$target || exit $$?; \
	done

test:
	for target in $(ALGOS_DIRS); do\
		make -C $$target test || exit $$?; \
	done

clean:
	for target in $(ALGOS_DIRS);\
	do\
		make -C $$target clean || exit $$?; \
	done
