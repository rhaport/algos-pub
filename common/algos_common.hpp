#if !defined(ALGOS_ALGOS_COMMON_HPP_)
#define ALGOS_ALGOS_COMMON_HPP_

/****************************************************************************
 * INCLUDE
 ****************************************************************************/
#include <vector>
#include <cstddef>
#include <iostream>

/****************************************************************************
 * FORWARD CLASS DECLARATION
 ****************************************************************************/

/****************************************************************************
 * EXPORTED TYPES
 ****************************************************************************/

/****************************************************************************
 * EXPORTED FUNCTIONS
 ****************************************************************************/
namespace algos
{
template<class Iterator>
using SortAlgo = void(*)(Iterator, Iterator);
}

template<class Type>
bool operator==(const std::vector<Type>& lhs, const std::vector<Type>& rhs )
{
    bool res = false;
    size_t size = lhs.size();

    if (size == rhs.size())
    {
        res = true;
        for (size_t i = 0; i < size && res; i++)
        {
            res &= (lhs[i] == rhs[i]);
        }
    }

    return res;
}

template <class Type>
std::ostream& operator<< (std::ostream& out, const std::vector<Type>& v)
{
    typename std::vector<Type>::const_iterator it;

    out << "[";
    for (it = v.begin(); it != v.end(); ++it)
    {
        out << *it;

        if ((it + 1) != v.end())
        {
            out << ", ";
        }
    }

    out << "]";

    return out;
}

template<class Iterator>
void dump_container(Iterator first, Iterator last)
{
    std::cout << "[";
    for (Iterator it = first; it != last; it++)
    {
        std::cout << *it;

        if ((it + 1) != last)
        {
            std::cout << ", ";
        }
    }

    std::cout << "]" << std::endl;
}

#endif /* ALGOS_ALGOS_COMMON_HPP_ */ 
