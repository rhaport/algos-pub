/*
 * test_sort_common.hpp
 *
 *  Created on: Nov 11, 2017
 *      Author: rha
 */

#if !defined(COMMON_TEST_SORT_COMMON_HPP_)
#define COMMON_TEST_SORT_COMMON_HPP_

/******************************************************************************
 * INCLUDES
 ******************************************************************************/
#include "algos_common.hpp"

#include <algorithm>
#include <cstdlib>
#include <vector>
/******************************************************************************
 * NAMESPACE
 ******************************************************************************/

/******************************************************************************
 * EXPORTED FUNCTIONS
 ******************************************************************************/
namespace algos
{
/****************************************************************************
 * @brief test template for sorting algorithms
 ****************************************************************************/
void test_sort(
        const std::vector<int> &v,
        SortAlgo<std::vector<int>::iterator> sort_algo)
{
    static int testCount = -1;

    std::vector<int> ref = v;
    std::vector<int> test = v;

    ++testCount;
    std::cout << "Enter test case #" << testCount << std::endl;

    // reference values
    std::sort(ref.begin(), ref.end());

    std::cout << "Before: " << test << std::endl;

    // algo under test
    sort_algo(test.begin(), test.end());

    std::cout << "After: " << test << std::endl;

    std::cout << "Verdict for test case #" << testCount << ": ";
    if (test == ref)
    {
         std::cout << "PASS" << std::endl;
    }
    else
    {
        std::cout << "FAIL" << std::endl;
        std::exit(EXIT_FAILURE);
    }
}
}

#endif /* COMMON_TEST_SORT_COMMON_HPP_ */
